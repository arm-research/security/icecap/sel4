#
# Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
#
# SPDX-License-Identifier: GPL-2.0-only
#

cmake_minimum_required(VERSION 3.7.2)

declare_platform(qemu-arm-virt KernelPlatformQEMUArmVirt PLAT_QEMU_ARM_VIRT KernelSel4ArchAarch64)

if(KernelPlatformQEMUArmVirt)
    declare_seL4_arch(aarch64)
    set(KernelArmCortexA57 ON)
    set(KernelArchArmV8a ON)
    config_set(KernelARMPlatform ARM_PLAT qemu-arm-virt)
    list(APPEND KernelDTSList "tools/dts/virt.dts")
    list(APPEND KernelDTSList "src/plat/qemu-arm-virt/overlay-qemu-arm-virt.dts")
    declare_default_headers(
        TIMER_FREQUENCY 62500000
        MAX_IRQ 211 # HACK for IceCap QEMU patches
        NUM_PPI 32
        TIMER drivers/timer/arm_generic.h
        INTERRUPT_CONTROLLER arch/machine/gic_v2.h
        CLK_MAGIC 4611686019llu
        CLK_SHIFT 58u
        KERNEL_WCET 10u
    )
endif()

add_sources(
    DEP "KernelPlatformQEMUArmVirt"
    CFILES src/arch/arm/machine/gic_v2.c src/arch/arm/machine/l2c_nop.c
)

config_string(
    KernelUserTop USER_TOP "Set seL4_UserTop constant"
    DEFAULT 0xa0000000
    UNQUOTE
    DEPENDS "KernelPlatformQEMUArmVirt;KernelSel4ArchAarch32"
)
